package retrofitAttempt.api;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofitAttempt.model.Post;


public interface JsonPlaceHolderApi {
    @GET("posts")
    Call<List<Post>> getPosts();
}